# Presence

* __exists -__ The field must exist, and not be `undefined`.
* __required -__ The field must exist, and not be `undefined`, `null` or an empty string.
* __empty -__ The field must be some kind of "empty". Things that are considered "empty" are as follows:
  * `""` (empty string)
  * `[]` (empty array)
  * `{}` (empty object)
  * Other falsey values

# Character set

* __alpha -__ `a-z`, `A-Z`
* __alphaNumeric -__ `a-z`, `A-Z`, `0-9`
* __alphaUnderscore -__ `a-z`, `A-Z`, `0-9`, `_`
* __alphaDash -__ `a-z`, `A-Z`, `0-9`, `_`, `-`

# Value

Length-related validators may apply to both strings and arrays.

* __exactLength:`length`-__ The value must have a length of exactly `length`.
* __minLength:`length` -__ The value must have a length of at least `length`.
* __maxLength:`length` -__ The value must have a length of at most `length`.
* __contains:`needle` -__ The value must contain the specified `needle` (applies to both strings and arrays).
* __accepted -__ Must be a value that indicates agreement - varies by language (defaulting to `en`):
  * __en, fr, nl -__ `"yes"`, `"on"`, `"1"`, `1`, `"true"`, `true`
  * __es -__ `"yes"`, `"on"`, `"1"`, `1`, `"true"`, `true`, `"si"`
  * __ru -__ `"yes"`, `"on"`, `"1"`, `1`, `"true"`, `true`, `"да"`

# Value (numbers)

Note that "numbers" refers to both Number-type values, and strings containing numeric values!

* __numeric -__ Must be a finite numeric value of some sort.
* __integer -__ Must be an integer value (either positive or negative).
* __natural -__ Must be a natural number (ie. an integer value of 0 or higher).
* __naturalNonZero -__ Must be a natural number, but *higher* than 0 (ie. an integer value of 1 or higher).
* __between:`min`:`max` -__ The value must numerically be between the `min` and `max` values (exclusive).
* __range:`min`:`max` -__ The value must numerically be *within* the `min` and `max` values (inclusive).
* __lessThan:`maxValue` -__ The value must numerically be less than the specified `maxValue` (exclusive).
* __lessThanEqualTo:`maxValue` -__ The value must numerically be less than *or equal to* the specified `maxValue` (inclusive).
* __greaterThan:`minValue` -__ The value must numerically be greater than the specified `minValue` (exclusive).
* __greaterThanEqualTo:`minValue` -__ The value must numerically be greater than *or equal to* the specified `minValue` (inclusive).

# Relations to other fields

* __matchesField:`field` -__ The value in this field must equal the value in the specified other `field`.
* __different:`field` -__ The value in this field must *not* equal the value in the specified other `field`.

# JavaScript types

* __NaN -__ Must be `NaN`.
* __null -__ Must be `null`
* __string -__ Must be a `String`.
* __number -__ Must be a `Number`.
* __array -__ Must be an `Array`.
* __plainObject -__ Must be a plain `object` (ie. object literal).
* __date -__ Must be a `Date` object.
* __function -__ Must be a `Function`.
* __regExp -__ Must be a `RegExp` object.
* __arguments -__ Must be an `arguments` object.

# Format

* __email -__ Must be a validly formatted e-mail address.
* __luhn -__ Must be a validly formatted creditcard number (according to a Luhn regular expression).
* __url -__ Must be a validly formatted URL.
* __ipv4 -__ Must be a validly formatted IPv4 address.
* __ipv6 -__ Must be a validly formatted IPv6 address.
* __uuid -__ Must be a validly formatted UUID.
* __base64 -__ Must be a validly formatted base64 string.
